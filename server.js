//Declarar las dependencias siempre para ser utilizadas
//Ejm: require('DEPENDENCIA');
var express = require('express');
// Dependencia para uso de ficheros
var userFile = require('./user.json');
//como hemos utilizado el RAW en formato json utilizamos esta bliblioteca
//esta en node_module
var bodyParser = require('body-parser');
//Declarar una variable para utilizar el objeto de la dependencia
//Ejm. VARDEPEND();
var app = express();
app.use(bodyParser.json()); // aqui dice que utilicemos el json

var totalUsers = 0;
//Variable contexto
const URL_BASE = '/apitechu/v1';
const PORT = process.env.PORT || 3003;

//Para llamar a un servicio es TIPO(URI,CALLBACK)
//Peticion GET de todos los 'users' (Collections)
app.get(URL_BASE + '/users',
    function (request,response) {
      console.log('GET '+ URL_BASE + 'users');
      response.send(userFile);
});

//Peticion GET de un 'users' (Instance)
app.get(URL_BASE + '/users/:id',
    function (request,response) {
      console.log('GET '+ URL_BASE + 'users/id');
      let indice = request.params.id;
      let instancia = userFile[indice-1];

      let respuesta = instancia != undefined ? instancia : {"mensaje":"Recurso no encontrado."};
      //CODIGO DE ESTAO HTTP
      response.status(200);
      response.send(respuesta);
      //console.log(request.params.id);
});

//* GET con Query String
app.get(URL_BASE + '/usersq',
        function(request,response){
          console.log('GET' + URL_BASE + ' con query string');
          console.log(request.query);
          response.send(respuesta);
});

//Peticiòn POST
app.post (URL_BASE + '/users',
          function(req,res){
          totalUSers = userFile.length;
          cuerpo = req.body;

          let newUser = {
                 userID : totalUSers,
                 first_name : req.body.first_name,
                 last_name : req.body.last_name,
                 email : req.body.email,
                 password : req.body.password
               };
               userFile.push(newUser);
               res.status(200);
               res.send({"mensaje":"Usuario creado con èxito.","userID":newUser , "body":req.body , "tamaño":String(cuerpo.length)});

});

//Peticiòn PUT
app.put(URL_BASE + '/users/:id',
   function(req,res){
     var cntBody = Object.keys(req.body).length;
     if (cntBody > 0) {
       let idx = req.params.id - 1
       if(userFile[idx] != undefined){
         userFile[idx].userID = req.body.Nombre;
         userFile[idx].last_name = req.body.Apellido;
         userFile[idx].email = req.body.email;
         userFile[idx].password = req.body.clave;
         res.status(201);
         res.send({"mensaje" : "Usuario actualizado con exito.",
                   "Array" : userFile[idx]});
         console.log(userFile[idx]);
       }
       else{
         res.send({"mensaje":"Recurso no encontrado."})
       }
     }
     else {
       let varError = {"mensaje" : "No Existe parametros para Body"};
       res.send(varError)
       console.log(varError);
     }
   });

//DELETE
app.delete(URL_BASE + '/users/:id',
   function(req,res){
       let idx = req.params.id - 1;
       if(userFile[idx] != undefined){
         userFile.splice(idx, 1);
         //res.status(200);
         res.send({"mensaje" : "Usuario eliminado con exito.","usuarios":userFile});
             }
       else{
         res.send({"mensaje":"Recurso no encontrado."})
       }


   });







//3000: puerto por defecto
app.listen(PORT,function(){
  console.log('Vamos al f');
});

//3000: puerto por defecto
//app.listen(3000);
//console.log('Sin callback2');
